package com.traffic.oauthloginapp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//import org.springframework.data.annotation.Id;
//@Document(collection = "UserConnection")

@Entity
@Table(name = "UserConnection")
public class UserConnection implements Serializable {
 
	private static final long serialVersionUID = 1L;
//  @Id
    @Id
    @Column(name = "Userid", length = 255, nullable = false)
    private String userId;
    
//  @Field(value = "Providerid") 
    @Id
    @Column(name = "Providerid", length = 255, nullable = false)
    private String providerId;
 
//  @Field(value = "Provideruserid")
    @Id
    @Column(name = "Provideruserid", length = 255, nullable = false)
    private String providerUserId;
    
//  @Field(value = "Rank")
    @Column(name = "Rank", nullable = false)
    private int rank;

//  @Field(value = "Displayname")
    @Column(name = "Displayname", length = 255, nullable = true)
    private String displayName;

//  @Field(value = "Profileurl")
    @Column(name = "Profileurl", length = 512, nullable = true)
    private String profileUrl;

//  @Field(value = "Imageurl")
    @Column(name = "Imageurl", length = 512, nullable = true)
    private String imageUrl;

//  @Field(value = "Accesstoken")
    @Column(name = "Accesstoken", length = 512, nullable = true)
    private String accessToken;

//  @Field(value = "Secret")
    @Column(name = "Secret", length = 512, nullable = true)
    private String secret;

//  @Field(value = "Refreshtoken")
    @Column(name = "Refreshtoken", length = 512, nullable = true)
    private String refreshToken;

//  @Field(value = "Expiretime")
    @Column(name = "Expiretime", nullable = true)
    private Long expireTime;
 
    public String getUserId() {
        return userId;
    }
 
    public void setUserId(String userId) {
        this.userId = userId;
    }
 
    public String getProviderId() {
        return providerId;
    }
 
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
 
    public String getProviderUserId() {
        return providerUserId;
    }
 
    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }
 
    public int getRank() {
        return rank;
    }
 
    public void setRank(int rank) {
        this.rank = rank;
    }
 
    public String getDisplayName() {
        return displayName;
    }
 
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
 
    public String getProfileUrl() {
        return profileUrl;
    }
 
    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }
 
    public String getImageUrl() {
        return imageUrl;
    }
 
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
 
    public String getAccessToken() {
        return accessToken;
    }
 
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
 
    public String getSecret() {
        return secret;
    }
 
    public void setSecret(String secret) {
        this.secret = secret;
    }
 
    public String getRefreshToken() {
        return refreshToken;
    }
 
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
 
    public Long getExpireTime() {
        return expireTime;
    }
 
    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }
 
}