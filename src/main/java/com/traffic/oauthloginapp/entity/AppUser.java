package com.traffic.oauthloginapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
//import org.springframework.data.annotation.Id;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
  
@Entity
@Table(name = "App_User", //
        uniqueConstraints = { //
                @UniqueConstraint(name = "APP_USER_UK", columnNames = "User_Name"),
                @UniqueConstraint(name = "APP_USER_UK2", columnNames = "Email") })
//@Document(collection = "App_User")
public class AppUser {
  
    @Id
    @GeneratedValue
    @Column(name = "User_Id", nullable = false)
//	@Id
    private Long userId;
  
    @Column(name = "User_Name", length = 36, nullable = false)
//    @Field(value = "User_Name")
    private String userName;
  
    @Column(name = "Email", length = 128, nullable = false)
//    @Field(value = "Email")
    private String email;
  
    @Column(name = "First_Name", length = 36, nullable = true)
//    @Field(value = "First_Name")
    private String firstName;
  
    @Column(name = "Last_Name", length = 36, nullable = true)
//    @Field(value = "Last_Name")
    private String lastName;
  
    @Column(name = "Encryted_Password", length = 128, nullable = false)
//    @Field(value = "Encryted_Password")
    private String encrytedPassword;
  
    @Column(name = "Enabled", length = 1, nullable = false)
//    @Field(value = "Enabled")
    private boolean enabled;
  
    public Long getUserId() {
        return userId;
    }
  
    public void setUserId(Long userId) {
        this.userId = userId;
    }
  
    public String getUserName() {
        return userName;
    }
  
    public void setUserName(String userName) {
        this.userName = userName;
    }
  
    public String getEmail() {
        return email;
    }
  
    public void setEmail(String email) {
        this.email = email;
    }
  
    public String getFirstName() {
        return firstName;
    }
  
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
  
    public String getLastName() {
        return lastName;
    }
  
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
  
    public String getEncrytedPassword() {
        return encrytedPassword;
    }
  
    public void setEncrytedPassword(String encrytedPassword) {
        this.encrytedPassword = encrytedPassword;
    }
  
    public boolean isEnabled() {
        return enabled;
    }
  
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
  
}