package com.traffic.oauthloginapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import org.springframework.data.annotation.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
  
@Entity
@Table(name = "User_Role", //
        uniqueConstraints = { //
                @UniqueConstraint(name = "USER_ROLE_UK", //
                        columnNames = { "User_Id", "Role_Id" }) })
//@Document(collection = "User_Role")
public class UserRole {
  
    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
//	@Id
    private Long id;
  
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "User_Id", nullable = false)
//    @Field(value = "User_Id")
    private AppUser appUser;
  
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Role_Id", nullable = false)
//    @Field(value = "Role_Id")
    private AppRole appRole;
  
    public Long getId() {
        return id;
    }
  
    public void setId(Long id) {
        this.id = id;
    }
  
    public AppUser getAppUser() {
        return appUser;
    }
  
    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }
  
    public AppRole getAppRole() {
        return appRole;
    }
  
    public void setAppRole(AppRole appRole) {
        this.appRole = appRole;
    }
  
}