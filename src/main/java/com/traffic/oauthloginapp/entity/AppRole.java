package com.traffic.oauthloginapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import org.springframework.data.annotation.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Entity
@Table(name = "App_Role", //
        uniqueConstraints = { //
                @UniqueConstraint(name = "APP_ROLE_UK", columnNames = "Role_Name") })
//@Document(collection = "App_Role")
public class AppRole {
       
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
     
    @Id
    @GeneratedValue
    @Column(name = "Role_Id", nullable = false)
//    @Id
    private Long roleId;
  
    @Column(name = "Role_Name", length = 30, nullable = false)
//    @Field(value = "Role_Name")
    private String roleName;
  
    public Long getRoleId() {
        return roleId;
    }
  
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
  
    public String getRoleName() {
        return roleName;
    }
  
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
     
}